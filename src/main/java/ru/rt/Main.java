package ru.rt;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

public class Main {
    static int expectedCount = 1000000;

    public static void main(String[] args) throws Exception {
        BloomFilter<Integer> filter = BloomFilter.create(
                Funnels.integerFunnel(),
                expectedCount,
                0.01);

        for (int i = 0; i < 1000000; i++) {
            filter.put(i);
        }

        int count = 0;
        for (int i = 1000001; i < 2000000; i++) {
            if(filter.mightContain(i)){
                count++;
            }
        }
        double roundOff = (double)count/(double)expectedCount ;
        System.out.println(String.format("%d ложноположительных результатов - это %.2f от общего числа", count,roundOff));
    }


}
